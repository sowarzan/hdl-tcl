# Welcome to the *hdl-tcl* project. #

## Supported software versions ##

| Vivado | 2018        | 2019       | 2020   | 2021   | 2022
|  ---   | ---         | ---        | ---    | ---    | ---
|        | .1 (tested) |.1          |.1      |.1      |.1
|        | .2          |.2 (tested) |.2      |.2      |.2
|        | .3 (tested) |            |        |        |

| Modelsim | 10           |
|  ---     | ---          |
|          | .7a (tested) |


| Rivera-PRO | 2021       | 2022
|  ---       | ---        | ---
|            |.4 (tested) |.10 (tested)

## How to use repository ##

1. Add repository like submodule to your project

```
git submodule add https://gitlab.cern.ch/sowarzan/hdl-tcl.git <destination_folder>
```
2. Create

    2.1 Create list of files for Vivado and simulator based on: [files.tcl](example/files.tcl)

    2.2 If you use IP Cores generated in Vivado IP Catalog, add script for instation are to: [ip_catalog.tcl](example/ip_catalog.tcl)

    2.3 Create Makefile in top dir of your project, if you don't support make you can use bash script; you can use template prepared in examples:

    * [Makefile](example/Makefile)
    * [make.sh](example/make.sh)


3. Use commands ```make <command>``` or ```./make.sh <command>``` in your .gitlab-ci.yml file,

| Command      | Description                          |
| ------------ | ------------------------------------ |
| test         | Display environment variable status  |
| sources      | Vivado create project                |
| ipmake       | Vivado the IP cores synthesis only   |
| sim          | Run simulation on selected simulator |
| vunit        | Run unit tests based on VUnit        |
| synth        | Vivado proejct synthesis only        |
| impl         | Vivado project implementation only   |
| all          | Vivado make all build                |
| clean        | Remove output and testbench dir      |

### Projects supported with *hdl-tcl* : ###

 - [EDA-02917-V2-0-DamperLoop](https://gitlab.cern.ch/BE-RF-PLDesign/PS/EDA-02917-V2-0-DamperLoop)

