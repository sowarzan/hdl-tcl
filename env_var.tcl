#######################################
# Title      : env_var
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : env_var.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for importing variables to software
#              from environmental variables
#
# Note: run on pc with predefined environmental variables
###############################################################

# Project Variables
set VIVADO_PROJECT   $::env(VIVADO_PROJECT)
set PRJ_PART         $::env(PRJ_PART)
set PRJ_LANG         $::env(PRJ_LANG)
set CORES_NO         $::env(CORES_NO)
set PROJ_DIR         $::env(PROJ_DIR)
set SRC_FILE         $::env(SRC_FILE)
set OUT_DIR          $::env(OUT_DIR)
set TCL_DIR          $::env(TCL_DIR)

# Allow to skip IP_CATALOG
if { [catch {set IP_CATALOG $::env(IP_CATALOG)}] == 0 } {
  set IP_CATALOG $::env(IP_CATALOG)
}
# Allow to skip simulation env var when simulation is disabled
if { [catch {set SIM_TOOL $::env(SIM_TOOL)}] == 0 } {
  set SIM_TOOL         $::env(SIM_TOOL)
  set SIM_LIB          $::env(SIM_LIB)
  set IP_PATH          $::env(IP_PATH)
  set TB_DIR           $::env(TB_DIR)
  set WORK_NAME        $::env(WORK_NAME)
  set VLOG_PARAM       $::env(VLOG_PARAM)
  set SIM_PARAM        $::env(SIM_PARAM)
}
