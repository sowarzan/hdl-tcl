#!/bin/bash
#######################################
# Title      : hdl-make
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : hdl-make.sh
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: bash script for executing tcl scripts based on user choice
#
###############################################################

### Default namespace based on SY-RF-FB CERN section namespace ###

# Define default project name if not set
[[ -z "${VIVADO_PROJECT}" ]] && export VIVADO_PROJECT=${PWD##*/}

# Define default target language to VHDL if not set
[[ -z "${PRJ_LANG}" ]] && export PRJ_LANG='VHDL'

# Read and set number of available cores if not set
[[ -z "${CORES_NO}" ]] && export CORES_NO=${cat /proc/cpuinfo | grep processor | wc -l}

# Set default path to project directory if not set
[[ -z "${PROJ_DIR}" ]] && export PROJ_DIR=${pwd}

# Set default path to working directory if not set
[[ -z "${OUT_DIR}" ]] && export OUT_DIR=${PROJ_DIR}/Vivado/Work

# Set default path to submodule with vivado-tcl repository  if not set
[[ -z "${TCL_DIR}" ]] && export TCL_DIR=${PROJ_DIR}/Vivado/Options/vivado-tcl

# If simulation is enabled, set default path to testbench directory if not set
[[ -z "${SIM_TOOL}" ]] && [[ -z "${TB_DIR}" ]] && export TB_DIR=${PROJ_DIR}/Vivado/Options/Testbenches/top

# If simulation is enabled, set default name for work library if not set
[[ -z "${SIM_TOOL}" ]] && [[ -z "${WORK_NAME}" ]] && export WORK_NAME=work

# If simulation is enabled, set default env variable to precompiled library for simulation if not set
[[ -z "${SIM_TOOL}" ]] && [[ -z "${SIM_LIB}" ]] && export SIM_LIB=${RIVIERAPRO_VIVADO_LIBS}

# If simulation is enabled, set default parameters for simulation if not set
[[ -z "${SIM_TOOL}" ]] && [[ -z "${SIM_PARAM}" ]] && export SIM_PARAM=-t 1ps ${WORK_NAME}.main -voptargs=+acc

##################################### FUNCTIONS ########################3

show_help () {
  echo " "
  echo "Help for hdl-tcl"
  echo " "
  echo "| Command      | Description                          |"
  echo "| ------------ | ------------------------------------ |"
  echo "| test         | Display environment variable status  |"
  echo "| sources      | Vivado create project                |"
  echo "| ipmake       | Vivado the IP cores synthesis only   |"
  echo "| sim          | Run simulation on selected simulator |"
  echo "| vunit        | Run unit tests based on VUnit        |"
  echo "| synth        | Vivado proejct synthesis only        |"
  echo "| impl         | Vivado project implementation only   |"
  echo "| all          | Vivado make all build                |"
  echo "| clean        | Remove output and testbench dir      |"
  echo "| -help, -h    | Dispaly this help                    |"
  echo " "
  echo "For more information visit: https://gitlab.cern.ch/sowarzan/hdl-tcl/"
  echo " "
}

#### Command declaration ####

# Catch command from input
COMMAND=$1

vaild_cmd=false

test -d ${OUT_DIR} || mkdir ${OUT_DIR}
test -d ${TB_DIR} || mkdir ${TB_DIR}

if [[ "${COMMAND}" == "" ]]; then
	show_help
	exit 0
fi

if [[ "${COMMAND}" == "test" ]]; then
	echo ------ RTL ENV VAR ------
	echo VIVADO_PROJECT: ${VIVADO_PROJECT}
	echo PRJ_PART:   ${PRJ_PART}
	echo PRJ_LANG:   ${PRJ_LANG}
	echo CORES_NO:   ${CORES_NO}
	echo PROJ_DIR:   ${PROJ_DIR}
	echo SRC_FILE:   ${SRC_FILE}
	echo OUT_DIR:    ${OUT_DIR}
	echo IP_CATALOG: ${IP_CATALOG}
	echo TCL_DIR:    ${TCL_DIR}
	if [[ "${SIM_TOOL}" != "" ]]; then
		echo ------ SIMULATION ENV VAR ------
		echo SIM_TOOL:   ${SIM_TOOL}
		echo SIM_LIB:    ${SIM_LIB}
		echo IP_PATH:    ${IP_PATH}
		echo TB_DIR:     ${TB_DIR}
		echo WORK_NAME:  ${WORK_NAME}
		echo VLOG_PARAM: ${VLOG_PARAM}
		echo SIM_PARAM:  ${SIM_PARAM}
	fi
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "sources" ]]; then
	echo "Vivado Source Setup"
	cd ${OUT_DIR}; vivado -mode batch -source ${TCL_DIR}/vivado/sources.tcl
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "ipmake" ]]; then
	echo "Vivado the IP cores synthesis Only"
	cd ${OUT_DIR}; vivado -mode batch -source ${TCL_DIR}/vivado/ip_make.tcl
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "sim" ]]; then
	echo "Simulation of project in ${SIM_TOOL}, batch-mode"
	cd ${TB_DIR}; test -d err_status.txt || rm -f err_status.txt
	if [[ "$SIM_TOOL" == "riviera" ]]; then
		SIM_PATH=$(find / 2>/dev/null -type f -iname '*runvsimsa*')
		${SIM_PATH} -do ${TCL_DIR}/simulation/compile.tcl
	else
		vsim -do ${TCL_DIR}/simulation/compile.tcl
	fi
	vaild_cmd=true
	[[ -f err_status.txt ]] && exit 1
fi

if [[ "${COMMAND}" == "synth" ]]; then
	echo "Vivado Synthesis Only"
	cd ${OUT_DIR}; vivado -mode batch -source ${TCL_DIR}/vivado/synth.tcl
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "impl" ]]; then
	echo "Vivado Implementation Only"
	cd ${OUT_DIR}; vivado -mode batch -source ${TCL_DIR}/vivado/impl.tcl
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "all" ]]; then
	echo "Vivado Make ALL"
	cd ${OUT_DIR}; vivado -mode batch -source ${TCL_DIR}/vivado/build.tcl
	vaild_cmd = true
fi

if [[ "${COMMAND}" == "clean" ]]; then
	rm -rf  ${OUT_DIR}
	rm -rf  ${TB_DIR}
	rm -rf  ${RUN_DIR}/vunit_out
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "hdl-clean" ]]; then
	rm -rf  ${OUT_DIR}
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "sim-clean" ]]; then
	rm -rf  ${TB_DIR}
	rm -rf  ${RUN_DIR}/vunit_out
	vaild_cmd=true
fi

if [[ "${COMMAND}" == "vunit" ]]; then
	echo "VUnit test based on Vunit"
	cd ${PROJ_DIR}/Vivado/Options/Testbenches/; ${TCL_DIR}/simulation/vunit.sh
	# Catch exit status from vunit.sh
	exit_status=$?
	# Check that vunit.sh execute with error
	if [ "${exit_status}" -ne 0 ]; then
    # If error exit with error
    exit ${exit_status}
	fi
  vaild_cmd=true
fi

if [ ${vaild_cmd} == false ]; then
	show_help
	exit 0
fi

