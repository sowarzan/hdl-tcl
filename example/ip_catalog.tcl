### IP Catalog blocks instantiation ###
create_ip -quiet -name ila -vendor xilinx.com -library ip -version 6.2 -module_name ila_0
set_property -dict [list CONFIG.C_PROBE0_WIDTH {16} CONFIG.C_DATA_DEPTH {8192} CONFIG.Component_Name {ila_0}] [get_ips ila_0]

create_ip -quiet -name ila -vendor xilinx.com -library ip -version 6.2 -module_name ila_1
set_property -dict [list CONFIG.C_PROBE0_WIDTH {16} CONFIG.C_DATA_DEPTH {8192} CONFIG.Component_Name {ila_1}] [get_ips ila_1]