#!/bin/bash

################################### RTL ENV VAR ###################################
# Define project name \
#e.g. PS_DamperLoops
export VIVADO_PROJECT=

# Define target part \
#e.g. xc7a200tffg1156-1
export PRJ_PART=

# Define target language Verilog or VHDL
export PRJ_LANG=Verilog

# Put number of available cores \
#e.g. 6
export CORES_NO=

### Using a non-standard target directory structure ###
### If you don't want use ENV VAR listed below just commented it ###

# Path to project directory
export PROJ_DIR=$(pwd)

# Path to file with filelist generated from vivado \
#\
# list of files which you want to add to project \
#e.g.$(PROJ_DIR)/Vivado/Options/files.tcl
export SRC_FILE=

# Path to IP Catalog blocks instantiation \
# e.g. $(PROJ_DIR)/Vivado/Options/ip_catalog.tcl
export IP_CATALOG=

# Path to working directory \
#e.g. $(abspath $(PROJ_DIR)/Vivado/Work)
export OUT_DIR=

# Path to submodule with vivado-tcl repository \
#e.g. $(PROJ_DIR)/Vivado/Options/hdl-tcl
export TCL_DIR=

################################### SIMULATION ENV VAR ###################################
# Choose simulator: modelsim: msim, riviera-PRO: riviera
export SIM_TOOL=riviera

# Env variable or path to precompiled library for simulation \
#e.g. $(RIVIERAPRO_VIVADO_LIBS) or /home/user/pldesign/riviera_libs
export SIM_LIB=

# Path to IPCore .xci files (can be generated from sysgen) \
#e.g. $(PROJ_DIR)/IPCores/Resampler/SysGen/Outputs/xc7a200tffg1156-1/Resampler_IP/ip/*/*.xci
export IP_PATH=

# Path to testbench directory \
#e.g. $(PROJ_DIR)/Vivado/Options/Testbenches/top
export TB_DIR=

# Set name for work library \
#e.g. work
export WORK_NAME=

# Set parameters for vlog \
#e.g. +incdir+$(PROJ_DIR)/IPCores/general-cores/sim+$(PROJ_DIR)/IPCores/vme64x-core/hdl/sim/vme64x_bfm
export VLOG_PARAM=

# Set parameters for simulation \
#e.g. -permit_unmatched_virtual_intf -t 1ps $(WORK_NAME).main $(WORK_NAME).glbl -L unisims_ver -voptargs=+acc
export SIM_PARAM=

# Use top level bash script
source ${TCL_DIR}/hdl-make.sh