# Get variables
source $::env(TCL_DIR)/env_var.tcl

# List of sources files
set sources_1 [ list \
              [ file normalize "${PROJ_DIR}/IPCores/<filename_1>" ]\
              [ file normalize "${PROJ_DIR}/IPCores/<filename_2>" ]\
              [ file normalize "${PROJ_DIR}/IPCores/<filename_3>" ]\
              [ file normalize "${PROJ_DIR}/IPCores/<filename_4>" ]\
              [ file normalize "${PROJ_DIR}/IPCores/<filename_5>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Sources/<filename_6>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Sources/<filename_7>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Sources/<filename_8>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Sources/<filename_9>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Sources/<filename_10>" ]\
              [ file normalize "${PROJ_DIR}/MemMap/<filename_11>" ]\
              [ file normalize "${PROJ_DIR}/MemMap/<filename_12>" ]\
              ]; list

# List of constraints files
set constrs_1 [ list \
              [ file normalize "${PROJ_DIR}/Vivado/Options/Constraints/<constraints_1>" ]\
              [ file normalize "${PROJ_DIR}/Vivado/Options/Constraints/<constraints_2>" ]\
              ]

# List of simulations files
set sims [ list \
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/glbl.v" ]\
         [ file normalize "${PROJ_DIR}/IPCores/<filename_1>" ]\
         [ file normalize "${PROJ_DIR}/IPCores/<filename_2>" ]\
         [ file normalize "${PROJ_DIR}/IPCores/<filename_3>" ]\
         [ file normalize "${PROJ_DIR}/IPCores/<filename_4>" ]\
         [ file normalize "${PROJ_DIR}/IPCores/<filename_5>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<filename_6>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<filename_7>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<filename_8>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<filename_9>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<filename_10>" ]\
         [ file normalize "${OUT_DIR}/${VIVADO_PROJECT}.srcs/sources_1/ip/<ip_name_1>" ]\
         [ file normalize "${OUT_DIR}/${VIVADO_PROJECT}.srcs/sources_1/ip/<ip_name_2>" ]\
         [ file normalize "${OUT_DIR}/${VIVADO_PROJECT}.srcs/sources_1/ip/<ip_name_3>" ]\
         [ file normalize "${OUT_DIR}/${VIVADO_PROJECT}.srcs/sources_1/ip/<ip_name_4>" ]\
         [ file normalize "${OUT_DIR}/${VIVADO_PROJECT}.srcs/sources_1/ip/<ip_name_5>" ]\
         [ file normalize "${PROJ_DIR}/Vivado/Options/Testbenches/include/<top_tb_name>" ]\
         ]; list