#!/bin/bash
#######################################
# Title      : hdl-make
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : hdl-make.mk
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: make script for expanding Makefile
#
###############################################################

### Default namespace based on SY-RF-FB CERN section namespace ###

# Define default project name if not set
ifndef VIVADO_PROJECT
export VIVADO_PROJECT=$(notdir $(PWD))
endif

# Define default target language to VHDL if not set
ifndef PRJ_LANG
export PRJ_LANG=VHDL
endif

# Read and set number of available cores if not set
ifndef CORES_NO
export CORES_NO=$(shell cat /proc/cpuinfo | grep processor | wc -l)
endif

# Set default path to current dir if not set
ifndef PROJ_DIR
export PROJ_DIR=$(abspath $(PWD))
endif

# Set default path to working directory if not set
ifndef OUT_DIR
export OUT_DIR=$(abspath $(PROJ_DIR)/Vivado/Work)
endif

# Set default path to submodule with vivado-tcl repository if not set
ifndef TCL_DIR
export TCL_DIR=$(PROJ_DIR)/Vivado/Options/vivado-tcl
endif

# If simulation is enabled
ifndef SIM_TOOL
else
# Set default path to testbench directory if not set
	ifndef TB_DIR
	export TB_DIR=$(PROJ_DIR)/Vivado/Options/Testbenches/top
	endif

# Set default name for work library if not set
	ifndef WORK_NAME
	export WORK_NAME=work
	endif

# Set default env variable to precompiled library for simulation if not set
	ifndef SIM_LIB
	export SIM_LIB=$(RIVIERAPRO_VIVADO_LIBS)
	endif

# Set default parameters for simulation if not set
	ifndef SIM_PARAM
	export SIM_PARAM=-t 1ps $(WORK_NAME).main -voptargs=+acc
	endif
endif

.DEFAULT_GOAL := help

help :
	@echo " "
	@echo "Help for hdl-tcl"
	@echo " "
	@echo "| Command      | Description                          |"
	@echo "| ------------ | ------------------------------------ |"
	@echo "| test         | Display environment variable status  |"
	@echo "| sources      | Vivado create project                |"
	@echo "| ipmake       | Vivado the IP cores synthesis only   |"
	@echo "| sim          | Run simulation on selected simulator |"
	@echo "| vunit        | Run unit tests based on VUnit        |"
	@echo "| synth        | Vivado proejct synthesis only        |"
	@echo "| impl         | Vivado project implementation only   |"
	@echo "| all          | Vivado make all build                |"
	@echo "| clean        | Remove output and testbench dir      |"
	@echo "| help         | Dispaly this help                    |"
	@echo " "
	@echo "For more information visit: https://gitlab.cern.ch/sowarzan/hdl-tcl/"
	@echo " "

test :
	@echo ------ RTL ENV VAR ------
	@echo VIVADO_PROJECT: $(VIVADO_PROJECT)
	@echo PRJ_PART:   $(PRJ_PART)
	@echo PRJ_LANG:   $(PRJ_LANG)
	@echo CORE_NO:    $(CORES_NO)
	@echo PROJ_DIR:   $(PROJ_DIR)
	@echo SRC_FILE:   $(SRC_FILE)
	@echo OUT_DIR:    $(OUT_DIR)
	@echo IP_CATALOG: $(IP_CATALOG)
	@echo TCL_DIR:    $(TCL_DIR)
ifndef SIM_TOOL
else
	@echo ------ SIMULATION ENV VAR ------
	@echo SIM_TOOL:   $(SIM_TOOL)
	@echo SIM_LIB:    $(SIM_LIB)
	@echo IP_PATH:    $(IP_PATH)
	@echo TB_DIR:     $(TB_DIR)
	@echo WORK_NAME:  $(WORK_NAME)
	@echo VLOG_PARAM: $(VLOG_PARAM)
	@echo SIM_PARAM:  $(SIM_PARAM)
endif

#### Make declaration ####
dir :
	@test -d $(OUT_DIR) || mkdir $(OUT_DIR)
	@test -d $(TB_DIR) || mkdir $(TB_DIR)

sources : dir
	$(call ACTION_HEADER,"Vivado Source Setup")
	@cd $(OUT_DIR); vivado -mode batch -source $(TCL_DIR)/vivado/sources.tcl

ipmake : dir
	$(call ACTION_HEADER,"Vivado the IP cores synthesis Only")
	@cd $(OUT_DIR); vivado -mode batch -source $(TCL_DIR)/vivado/ip_make.tcl

sim : dir
	$(call ACTION_HEADER,"Simulation of project in $(SIM_TOOL)")
ifeq ($(SIM_TOOL),riviera)
			@cd $(TB_DIR); $(shell find / 2>/dev/null -type f -iname '*runvsimsa*') -do $(TCL_DIR)/simulation/compile.tcl
else
			@cd $(TB_DIR); vsim -do $(TCL_DIR)/simulation/compile.tcl
endif

synth : dir
	$(call ACTION_HEADER,"Vivado Synthesis Only")
	@cd $(OUT_DIR); vivado -mode batch -source $(TCL_DIR)/vivado/synth.tcl

impl : dir
	$(call ACTION_HEADER,"Vivado Implementation Only")
	@cd $(OUT_DIR); vivado -mode batch -source $(TCL_DIR)/vivado/impl.tcl

all : dir
	$(call ACTION_HEADER,"Vivado Make ALL")
	@cd $(OUT_DIR); vivado -mode batch -source $(TCL_DIR)/vivado/build.tcl

clean :
	rm -rf $(OUT_DIR)
	rm -rf $(TB_DIR)
	rm -rf $(RUN_DIR)/vunit_out

sim-clean :
	rm -rf $(TB_DIR)
	rm -rf $(RUN_DIR)/vunit_out

hdl-clean :
	rm -rf $(OUT_DIR)

vunit :
	$(call ACTION_HEADER,"Unit test based on Vunit")
	@cd $(PROJ_DIR)/Vivado/Options/Testbenches/; ${TCL_DIR}/simulation/vunit.sh