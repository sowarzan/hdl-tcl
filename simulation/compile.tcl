#######################################
# Title      : compile
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : compile.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for compiling and executing
#              fpga testbench using riviera-pro or modelsim
#
# Note: run on pc with preinstaled riviera-pro or modelsim
###############################################################

# Get variables
source $::env(TCL_DIR)/env_var.tcl

# Procedure for creating err_status.txt file when we catch error during simulation
proc writeError { } {
  if {[file isfile err_status.txt] == 0} {
    set f [open err_status.txt w]
    puts $f "1"
    close $f
  }
}

# Load project sources, simulation and ip_cores files
if { [catch {set SRC_FILE}] == 0 } {
  do ${SRC_FILE}
} else {
  puts "\n\n\n             File with name 'SRC_FILE' don't exsit             \n\n"
  puts "\n\n Replace SRC_FILE name in Make file in top dir of this project \n\n\n"
  writeError
  quit
}

# Set files to library
if { [catch {set sources_1}] == 0 } { set design_library ${sources_1} } else { set sources_1 0}
if { [catch {set sims}]      == 0 } { set test_library   ${sims}      } else { set sims      0}

# Set library list
set library_file_list [ list \
                                design_library [set design_library ${sources_1}]\
                                test_library   [set test_library ${sims}]\
                              ]

# Get simulation start time
set start_time [clock seconds]

# If we use riviera-pro map needed precompiled library
if { ${SIM_TOOL} == "riviera" } {
  amap -link ${SIM_LIB}
}

# If last compile time don't exsist (first compile), set last compile time to 0
if [catch {set last_compile_time}] {
  set last_compile_time 0
}

# Run procedure for compile out of date files
if { [catch {
  # Check if SRC_FILE is newest than last compile time
  if { ${last_compile_time} < [file mtime ${SRC_FILE}] } {
    vlib ${WORK_NAME}
    vmap xil_defaultlib ${WORK_NAME}
    foreach {library file_list} ${library_file_list} {
      vmap ${library} ${WORK_NAME}
      foreach file ${file_list} {
        if [regexp {.vhdl?$} ${file}] {
          vcom -quiet -93 -work ${WORK_NAME} ${file}
        } else {
          vlog -work ${WORK_NAME} -quiet -sv -sv ${VLOG_PARAM} ${file}
        }
      }
    }
  } else {
    # Check if library WORK_NAME exsist, if not create and add all files from SRC_FILE
    if [catch {vdir}] {
      foreach {library file_list} ${library_file_list} {
        vlib ${WORK_NAME}
        vmap xil_defaultlib ${WORK_NAME}
        vmap ${library} ${WORK_NAME}
        foreach file ${file_list} {
          if [regexp {.vhdl?$} ${file}] {
            vcom -quiet -93 -work ${WORK_NAME} ${file}
          } else {
            vlog -work ${WORK_NAME} -quiet -sv -sv ${VLOG_PARAM} ${file}
          }
        }
      }
    } else {
      # If library WORK_NAME exsist, recompile only files (from SRC_FILE) newest than compile time
      vmap xil_defaultlib ${WORK_NAME}
      foreach {library file_list} ${library_file_list} {
        vmap ${library} ${WORK_NAME}
        foreach file ${file_list} {
          if { ${last_compile_time} < [file mtime ${file}] } {
            if [regexp {.vhdl?$} ${file}] {
              vcom -quiet -93 -work ${WORK_NAME} ${file}
            } else {
              vlog -work ${WORK_NAME} -quiet -sv -sv ${VLOG_PARAM} ${file}
            }
          }
        }
      }
    }
  }
}] != 0 } {
  puts "\n\n\nError(s) were loaded the files!!!\n\n"
  puts "\n\nSimulation is incompleted due to the Error(s)\n\n"
  writeError
  quit
}
puts "Add files finished"

# Get new last compile time
set last_compile_time ${start_time}

# Map xil_defaultlib to work library for sysgen files
vmap xil_defaultlib ${WORK_NAME}

# Load the simulation
if { [catch {eval vsim -quiet ${SIM_PARAM}}] != 0 } {
  puts "\n\n\nError(s) were loaded the simulation!!!\n\n"
  puts "\n\nSimulation is incompleted due to the Error(s)\n\n"
  writeError
  quit
}

# If waves file is required
if { [catch {set SRC_WAVE}] == 0 } {
  do ${SRC_WAVE}
} else {
  add wave -position end sim:/main/*
}

# Run the simulation
run -all

# How long since project began?
set total_time [expr ([clock seconds]-${start_time})/60]
puts "Project took ${total_time} minutes"

# Quit simulation
quit -sim

# Quit simulator
quit