#!/bin/bash
#######################################
# Title      : vunit
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : vunit.sh
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: bash script preparing libs for rivierapro for
#              vunit unit test and run vunit test
#
# Note: run on pc with preinstaled vunit and riviera-pro
###############################################################

# Find path to riviera-pro vlib
VLIB_PATH=$(find / 2>/dev/null -type d -iname 'vlib')

# Set path to library.cfg
CFG_FILE=${VLIB_PATH}/library.cfg

# Prepared string to add to CFG_FILE
PART_A='$INCLUDE = "'
PART_B="$RIVIERAPRO_VIVADO_LIBS"
PART_C='/library.cfg"'
LIB_INCLUDE=$PART_A$PART_B$PART_C

# Read last line from CFG_FILE
LAST_LINE=$( tail -n 1 ${CFG_FILE} )

# Check that LIB_INCLUDE line exist in CFG_FILE if not add
if [[ "$LIB_INCLUDE" != "$(grep -F "$LIB_INCLUDE" $CFG_FILE)" ]]; then
  # Add new line to the end of CFG_FILE
  echo -e "" >> ${VLIB_PATH}/library.cfg
  # Add LIB_INCLUDE string to last line in CFG_FILE
  echo -e -n "$LIB_INCLUDE" >> ${VLIB_PATH}/library.cfg
fi

# cd to run.py directory
cd ${RUN_DIR}
# Set execute rights for run.py file
chmod 744 run.py;
# Execute Vunit test
./run.py --exit-0 --xunit-xml transcript.xml

# Catch exit status from run.py
exit_status=$?
# Check that run.py execute with error
if [ "${exit_status}" -ne 0 ]; then
    # If error exit with error
    exit ${exit_status}
fi

# Check errors insisde transcript.xml
ERR_MES=$(grep Error: transcript.xml)
if [ ! -z "$ERR_MES" ]; then
  # Check that unit test is allowed to fail
  if echo "$ERR_MES" | grep -q "expected to fail"; then
    # If unit test expected to fail exit without error
    exit 0
  else
    # If unit test not expected to fail exit with error
    exit 1
  fi
fi

# If pass exit without error
exit 0