#######################################
# Title      : synth
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : synth.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for making synthesis,
#              based on vivado project
#
# Note: run on pc with preinstaled Vivado software
###############################################################

# Get variables
source -quiet $::env(TCL_DIR)/env_var.tcl
# Get procedures
source -quiet $::env(TCL_DIR)/vivado/procs.tcl

# Open project
open_project -quiet ${VIVADO_PROJECT}
puts "Project opened"

# Update the complie order
update_compile_order -quiet -fileset sources_1

# Run procedure for checking that we don't miss any files
if { [FilesExist] != true } {
  exit -1
}

# Check if we need to stop the implement
reset_run impl_1
# Check if we need to stop the synthesis
reset_run synth_1

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part ${PRJ_PART} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
}

# set the current synthesis run
current_run -synthesis [get_runs synth_1]
# Make synthesis
launch_runs synth_1 -jobs ${CORES_NO}
# Wait fot synthesis finish
wait_on_run synth_1


set synthLog "${OUT_DIR}/${VIVADO_PROJECT}.runs/synth_1/runme.log"
if { [file exists ${synthLog}] == 1 } {
  set NumErr [llength [lsearch -all -regexp [split [read [open ${synthLog}]]] "^ERROR:"]]
  if { ${NumErr} != 0 } {
    puts "\n\n\nError(s) were detected during synthesis!!!\n\n"
    puts "\n\nSynthesize is incompleted due to the Error(s)\n\n"
    exit -1
  }
}
# Open synthesized schemat
open_run synth_1 -name synth_1

# Get the number of errors and multi-driven nets during synthesis
set MDRV [report_drc -quiet -checks {MDRV-1}]
if { ${MDRV} != 0 } {
  puts "\n\n\nMulti-driven nets detected during synthesis!!!\n\n"
  exit -1
}

# Close synthesized schemat
close_design

puts "${VIVADO_PROJECT} SYNTHESIS DONE"
# Close project
close_project