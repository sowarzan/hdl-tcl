#######################################
# Title      : sources
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : sources.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for making vivado project,
#              based on hdl files
#
# Note: run on pc with preinstaled Vivado software
###############################################################

# Get variables
source -quiet $::env(TCL_DIR)/env_var.tcl

# Check the project exist if not create new project
if { [file exists ${VIVADO_PROJECT}.xpr]} {
   open_project -quiet ${VIVADO_PROJECT}
} else {
   # Create a Project
   create_project ${VIVADO_PROJECT} -force ${OUT_DIR} -part ${PRJ_PART}
}

# Set a preferred language
set_property target_language ${PRJ_LANG} [current_project]

# Inport files to project
source ${SRC_FILE}

# Add sources files to sources_1
add_files -fileset sources_1 -norecurse $sources_1

# Add constraints files to constrs_1
add_files -fileset constrs_1 -norecurse $constrs_1

# Import IP Core files if any
if { ([catch {set IP_PATH}] == 0) && ([set IP_PATH] != "")} {
  import_files [glob "${IP_PATH}"]
}

# Generate from IP Catalog if any
if { ([catch {set IP_CATALOG}] == 0) && ([set IP_CATALOG] != "")} {
  source ${IP_CATALOG}
}

# Set top file
set obj [get_filesets sources_1]
set_property -name "top" -value "${VIVADO_PROJECT}" -objects $obj

# use VHDL-2008 on files that benefit from it
set_property file_type {VHDL 2008} [get_files -filter {FILE_TYPE == VHDL}]

# Update the complie order
update_compile_order -fileset sources_1

puts "${VIVADO_PROJECT} PROJECT CREATED"

# Close project
close_project