#######################################
# Title      : build
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : build.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for bitstream ceration,
#              based on hdl source files using Vivado
#
# Note: run on pc with preinstaled Vivado software
###############################################################

# Get variables
source -quiet $::env(TCL_DIR)/env_var.tcl

# Create project
source ${TCL_DIR}/vivado/sources.tcl

# Open project
open_project -quiet ${VIVADO_PROJECT}
puts "Project opened"

# Update the complie order
update_compile_order -quiet -fileset sources

# Check the project has ip cores if yes make ip build
open_project -quiet ${VIVADO_PROJECT}
if { [get_ips] != "" } {
  # Build IP cores
  source ${TCL_DIR}/vivado/ip_make.tcl
}

# Make synthesis
source ${TCL_DIR}/vivado/synth.tcl

# Make implementation
source ${TCL_DIR}/vivado/impl.tcl

puts "${VIVADO_PROJECT} BUILD DONE"


