#######################################
# Title      : impl
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : impl.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for making implementation only,
#              based on results of synthesis of project using Vivado
#
# Note: run on pc with preinstaled Vivado software
###############################################################

# Get variables
source -quiet $::env(TCL_DIR)/env_var.tcl
# Get procedures
source -quiet $::env(TCL_DIR)/vivado/procs.tcl

# Open project
open_project -quiet ${VIVADO_PROJECT}
puts "Project opened"

# Update the complie order
update_compile_order -quiet -fileset sources_1

# Run procedure for checking that we don't miss any files
if { [FilesExist] != true } {
  exit -1
}
puts "Files exist checking finished"

# Check if we need to stop the implement
reset_run impl_1

# Make implementation
launch_runs impl_1 -to_step write_bitstream -jobs ${CORES_NO}
# Wait fot implementation finish
wait_on_run impl_1

set implLog "${OUT_DIR}/${VIVADO_PROJECT}.runs/impl_1/runme.log"
if { [file exists ${implLog}] == 1 } {
  set NumErr [llength [lsearch -all -regexp [split [read [open ${implLog}]]] "^ERROR:"]]
  if { ${NumErr} != 0 } {
    puts "\n\n\nError(s) were detected during implementation!!!\n\n"
    puts "\n\nImplementation is incompleted due to the Error(s)\n\n"
    exit -1
  }
}
puts "Implementation finished"

# Run procedure for checking timing
[CheckTiming]
puts "Check Timing finished"


puts "${VIVADO_PROJECT} IMPLEMENTATION DONE"
# Close project
close_project
