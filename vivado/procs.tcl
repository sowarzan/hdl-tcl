#######################################
# Title      : procs
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : procs.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl library with procedures, these procedures
#              can be used in other tcl scripts
#
###############################################################

# Procedure for Elaboration Design for checking that we don't miss any files
proc FilesExist { } {
  # Get variables
  source -quiet $::env(TCL_DIR)/env_var.tcl

  puts "File Exist procedure"

  # Check if project contains a IP Cores
  set IPlist [get_ips]
  if { ${IPlist} != "" } {
    # Check if IP Cores are generated
    if { [validate_ip -quiet [get_ips] ] != 1 } {
      # Regenerate IP Cores
      source ${TCL_DIR}/ipmake.tcl
    }
  }

  # Run Elaboration Design
  synth_design -rtl -name rtl_1

  # Close Elaboration Design
  close_design

  # Check errors during Elaboration Design
  set mainLog "${OUT_DIR}/vivado.log"
  if { [file exists ${mainLog}] == 1 } {
    set NumErr [llength [lsearch -all -regexp [split [read [open ${mainLog}]]] "^ERROR:"]]
    if { ${NumErr} != 0 } {
      puts "\n\n\nError(s) were detected during Elaboration Design!!!\n\n"
      puts "\n\nElaboration Design is incompleted due to the Error(s)\n\n"
      return false
    }
    return true
  } else {
    return false
  }
}

# Procedure for Checking timing for checking Negative Slack and Hold Slack are met
proc CheckTiming { } {
  # Get variables
  source -quiet $::env(TCL_DIR)/env_var.tcl

  puts "Checking timing procedure"

  # Get timing value from impelmentation
  set wns [get_property STATS.WNS [get_runs [current_run]]]
  set tns [get_property STATS.TNS [get_runs [current_run]]]
  set whs [get_property STATS.WHS [get_runs [current_run]]]
  set ths [get_property STATS.THS [get_runs [current_run]]]

  # Check that Negative Slack and Hold Slack are met
  set IMPL_DIR [get_property DIRECTORY [get_runs impl_1]]
  if {$wns >= 0 && $whs >= 0} {
    puts "Time requirements were met"
    set status_file [open "${IMPL_DIR}/timing_report.txt" "w"]
    puts $status_file "Timing requirements were met.\n"
  } else {
    puts "Time requirements were NOT met"
    set status_file [open "${IMPL_DIR}/timing_report.txt" "w"]
    puts $status_file "Timing requirements were **NOT** met.\n"
  }

  # Save WNS, TNS, WHS, THS to file
  puts $status_file "WNS = $wns"
  puts $status_file "TNS = $tns"
  puts $status_file "WHS = $whs"
  puts $status_file "THS = $ths"
  puts $status_file "\n"
  close $status_file
}



