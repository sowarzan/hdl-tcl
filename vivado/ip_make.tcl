#######################################
# Title      : ip_make
# Project    : hdl-tcl
# Date       : 2023-03-11
# File       : ip_make.tcl
# Author     : Sebastian Owarzany
# Company    : CERN
#######################################

###############################################################
# Description: tcl script for building ip cores in vivado project
#
# Note: run on pc with preinstaled Vivado software
###############################################################

# Get variables
source -quiet $::env(TCL_DIR)/env_var.tcl
# Get procedures
source -quiet $::env(TCL_DIR)/vivado/procs.tcl

# Open project
open_project -quiet ${VIVADO_PROJECT}
puts "Project opened"

# Update the complie order
update_compile_order -quiet -fileset sources

# Build the all IP Core if not ready
synth_ip -quiet [get_ips]

# Refresh the project
update_compile_order -quiet -fileset sources_1

# Perform DRC check on IP to ensure that it is properly constructed
if { [validate_ip -quiet [get_ips]] != 1 } {
  exit -1
}

# run Elaboration Design for checking that we don't miss any files
synth_design -rtl -name rtl_1
# close Elaboration Design
close_design

puts "${VIVADO_PROJECT} IP CORES DONE"

# Close project
close_project